//
//  DetailsViewController.swift
//  SuperHero
//
//  Created by Macbook Pro CTO on 2021. 04. 27..
//

import Foundation
import UIKit

class DetailsViewController: UIViewController {
    
    
    var heroNameOnDetailsView = ""
    var descriptionOfHero = ""
    var intelligencePoint = ""
    var combatPoint = ""
    var powerPoint = ""
    var imageOfTheHeroinDetailsString = ""
    
    override func viewDidLoad() {

        navigationItem.title = heroNameOnDetailsView
        
        nameOfHERO.text = heroNameOnDetailsView
        imageOfTheSelectedHero.sd_setImage(with: URL(string: imageOfTheHeroinDetailsString), placeholderImage: UIImage(named: "placeholder5"))
        
        if powerPoint == "null" {
            powerNumber.isHidden = true
        } else {
            powerNumber.text = powerPoint
        }
        
        if intelligencePoint == "null" {
            intelligenceNumber.isHidden = true
        } else {
            intelligenceNumber.text = intelligencePoint
        }
        
        if combatPoint == "null" {
            combatNumber.isHidden = true
        } else {
            combatNumber.text = combatPoint
        }

    }
    
    @IBOutlet weak var imageOfTheSelectedHero: UIImageView! {
        didSet{
            rounded(viewForRound: imageOfTheSelectedHero)
        }
    }
    @IBOutlet weak var nameOfHERO: UILabel!
    
    
    @IBOutlet weak var powerNumber: UILabel!
    
    @IBOutlet weak var intelligenceNumber: UILabel!
    @IBOutlet weak var combatNumber: UILabel!
    @IBOutlet weak var details: UIView! {
        didSet{
            rounded(viewForRound: details)
        }
    }

    @IBOutlet weak var stack1: UIView! {
        didSet{
            rounded(viewForRound: stack1)
        }
    }
    
    @IBOutlet weak var stackView3rd: UIView! {
        didSet{
            rounded(viewForRound: stackView3rd)
        }
    }
    @IBOutlet weak var stackView2nd: UIView! {
        didSet{
            rounded(viewForRound: stackView2nd)
        }
    }
    func rounded(viewForRound: UIView) {
        viewForRound.layer.cornerRadius = 10
        viewForRound.layer.shadowOpacity = 1
        viewForRound.layer.shadowRadius = 2
        viewForRound.layer.shadowColor = CGColor(red: 0.6078, green: 0.6078, blue: 0.6078, alpha: 1.0)
        viewForRound.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewForRound.layer.masksToBounds = false
        viewForRound.clipsToBounds = true

    }

}
