//
//  HeroesTableViewController.swift
//  SuperHero
//
//  Created by Macbook Pro CTO on 2021. 04. 26..
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import SegueManager

class HeroesTableViewController: UITableViewController,  SeguePerformer {
    lazy var segueManager: SegueManager = {
      return SegueManager(viewController: self)
    }()

    var herosJsonArray:[JSON] = []

    
    
    override func viewDidLoad() {

        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(HeroesTableViewController.back(sender:)))
                self.navigationItem.leftBarButtonItem = newBackButton
    }
    
 
    
    @objc func back(sender: UIBarButtonItem) {
    
            _ = navigationController?.popViewController(animated: true)
        }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segueManager.prepare(for: segue)
  
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell = tableView.dequeueReusableCell(withIdentifier: "heroCell\(indexPath.row)") as! HeroesCellViewTableViewCell
            
        
        
            cell.nameOfTheHero.text = herosJsonArray[indexPath.row]["name"].string
            cell.imageOfTheHero.sd_setImage(with: URL(string: "\(herosJsonArray[indexPath.row]["image"]["url"])"), placeholderImage: UIImage(named: "placeholder5"))
        
        if herosJsonArray[indexPath.row]["biography"]["full-name"].string == "null" {
            cell.fullName.isHidden = true
        } else {
            cell.fullName.text = herosJsonArray[indexPath.row]["biography"]["full-name"].string
        }
           cell.publisherName.text = herosJsonArray[indexPath.row]["biography"]["publisher"].string
        
        if herosJsonArray[indexPath.row]["appearance"]["race"].string == "null" {
            cell.fullName.isHidden = true
            cell.raceOfTheHero.text = ""
        }else {
            cell.raceOfTheHero.text = herosJsonArray[indexPath.row]["appearance"]["race"].string
        }
            
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "goToDetails") { (details: DetailsViewController) in
            details.heroNameOnDetailsView = self.herosJsonArray[indexPath.row]["name"].string!
            print(details.heroNameOnDetailsView)
            details.imageOfTheHeroinDetailsString = self.herosJsonArray[indexPath.row]["image"]["url"].string!
            details.powerPoint = self.herosJsonArray[indexPath.item]["powerstats"]["power"].string!
            details.combatPoint = self.herosJsonArray[indexPath.item]["powerstats"]["combat"].string!
            details.intelligencePoint = self.herosJsonArray[indexPath.item]["powerstats"]["combat"].string!
        }
    }


    func randomSequenceGenerator(min: Int, max: Int) -> () -> Int {
        var numbers: [Int] = []
        return {
            if numbers.isEmpty {
                numbers = Array(min ... max)
            }

            let index = Int(arc4random_uniform(UInt32(numbers.count)))
            return numbers.remove(at: index)
        }
    }



}



    
    

