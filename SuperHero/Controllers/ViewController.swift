//
//  ViewController.swift
//  SuperHero
//
//  Created by Macbook Pro CTO on 2021. 04. 26..
//

import UIKit
import Alamofire
import SwiftyJSON
import Lottie

class ViewController: UIViewController {
    var id = 0
    var name:[String] = []
    var randomNumbersVC:[Int] = []
    var jsonArray:[JSON] = []
    var networkingResponse: [Data?] = []
    private var animationView: AnimationView?

   
    override func viewDidLoad() {
        super.viewDidLoad()
        animationView = .init(name: "16732-super-hero-charging")
          
          animationView!.frame = animView.bounds
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
        
        animationView!.animationSpeed = 0.5
         
         animView.addSubview(animationView!)
        animationView!.play()
        spinner.isHidden = true
        getHeroesButton.backgroundColor = .systemPink
        getHeroesButton.layer.cornerRadius = 10
        getHeroesButton.layer.masksToBounds = true
        getHeroesButton.layer.shadowOpacity = 1
        getHeroesButton.layer.shadowRadius = 2
        getHeroesButton.layer.shadowColor = CGColor(red: 0.6078, green: 0.6078, blue: 0.6078, alpha: 1.0)
        getHeroesButton.layer.shadowOffset = CGSize(width: 3, height: 3)
        getHeroesButton.layer.masksToBounds = false
        getHeroesButton.clipsToBounds = true
        getHeroesButton.titleLabel?.text = "GIVE ME 10 HEROS"

    }
    @IBOutlet weak var getHeroesButton: UIButton!
    
    @IBOutlet weak var animView: UIView!
    override func viewDidDisappear(_ animated: Bool) {
        spinner.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToHeros" {
            let vc = segue.destination as! HeroesTableViewController
            vc.herosJsonArray.removeAll()
            vc.herosJsonArray = jsonArray
           
            jsonArray.removeAll()
            randomNumbersVC.removeAll()

        }
    }
    
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    @IBAction func goToHerosPage(_ sender: Any) {
        UIView.animate(withDuration: 1.3,
                    animations: {
                        self.getHeroesButton.frame = CGRect(origin: self.getHeroesButton.center, size: CGSize(width: 0.2, height: 0.2))
                    },
                    completion: { _ in
                        UIView.animate(withDuration: 3.0) {
                            self.getHeroesButton.alpha = CGFloat(1)
                        }
                    })
        spinner.isHidden = false
        spinner.startAnimating()
        let getRandom = randomSequenceGenerator(min: 1, max: 700)
        for _ in 1..<11 {
            randomNumbersVC.append(getRandom())
        }
        print(randomNumbersVC)
        

        for index in 0...9 {
            AF.request("https://superheroapi.com/api/10209083123832850/\(randomNumbersVC[index])")
                    .responseJSON { response in
                        switch response.result {
                        case .success(let data) :
                            let value = JSON(data)
                            self.jsonArray.append(value)
                            DispatchQueue.main.async {
                                if self.jsonArray.count == 10 {
                                    self.spinner.stopAnimating()
                                    self.performSegue(withIdentifier: "goToHeros", sender: sender)

                                }
                            }
                          
                        case .failure(let error) :
                            print(error)
                        }

                    }
        }


    
}
    func randomSequenceGenerator(min: Int, max: Int) -> () -> Int {
        var numbers: [Int] = []
        return {
            if numbers.isEmpty {
                numbers = Array(min ... max)
            }

            let index = Int(arc4random_uniform(UInt32(numbers.count)))
            return numbers.remove(at: index)
        }
    }
}
