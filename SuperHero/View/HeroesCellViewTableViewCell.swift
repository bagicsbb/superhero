//
//  HeroesCellViewTableViewCell.swift
//  SuperHero
//
//  Created by Macbook Pro CTO on 2021. 04. 27..
//

import UIKit

class HeroesCellViewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    

    @IBOutlet weak var imageOfTheHero: UIImageView! {
        didSet {
            imageOfTheHero.layer.borderWidth = 1.0
            imageOfTheHero.layer.masksToBounds = false
            imageOfTheHero.layer.borderColor = UIColor.white.cgColor
            imageOfTheHero.layer.cornerRadius = 10
            imageOfTheHero.clipsToBounds = true
        }
    }
    @IBOutlet weak var nameOfTheHero: UILabel!
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = 10
            containerView.layer.shadowOpacity = 1
            containerView.layer.shadowRadius = 2
            containerView.layer.shadowColor = CGColor(red: 0.6078, green: 0.6078, blue: 0.6078, alpha: 1.0)
            containerView.layer.shadowOffset = CGSize(width: 3, height: 3)

        }

    }
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var publisherName: UILabel!
    @IBOutlet weak var raceOfTheHero: UILabel!

    @IBOutlet weak var titleView: UIView! {
        didSet {
            titleView.layer.cornerRadius = 10
            titleView.layer.shadowOpacity = 1
            titleView.layer.shadowRadius = 2
            titleView.layer.shadowColor = CGColor(red: 0.6078, green: 0.6078, blue: 0.6078, alpha: 1.0)
            titleView.layer.shadowOffset = CGSize(width: 3, height: 3)
            titleView.layer.masksToBounds = false
            titleView.clipsToBounds = true

        }
}
}
